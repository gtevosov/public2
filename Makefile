# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vpupkin <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/06 16:18:35 by vpupkin           #+#    #+#              #
#    Updated: 2017/11/09 16:16:24 by vpupkin          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

GCC = gcc -Wall -Wextra -Werror -I $(HDIR) 
NAME = bsq
HDIR = ./includes/
SRC =  ./srcs/
OBJ = *.o
ALLSRC = *.c

all: $(NAME)

$(NAME):
		$(GCC) -c $(SRC)$(ALLSRC)
		gcc $(OBJ) -o $(NAME)
clean:
		/bin/rm -f *.o

fclean: clean 
		/bin/rm -f $(NAME)

re: fclean all
