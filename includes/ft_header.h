/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_header.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vpupkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 15:58:42 by vpupkin           #+#    #+#             */
/*   Updated: 2017/11/15 23:17:27 by vpupkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_HEADER_H
# define FT_HEADER_H

# include <sys/types.h>
# include <sys/stat.h>
# include <unistd.h>
# include <fcntl.h>
# include <stdlib.h>

typedef struct		s_parse_settings
{
	unsigned int	width;
	int				lines;
	int				fd;
	int				em;
	int				ob;
	int				rp;						
}					t_parse_settings;

int					count_pos_value(int **array, int i, int j);
int					**create_empty_array(int width, int lines);
int					*find_bsq(int **array, int width, int lines);
int					min_var(int a, int b, int c);
void				ft_putstr(char *str);
char				*get_params(char *filename, int param);
unsigned int		get_width(char *filename);
int					ft_atoi(char *str);
void				ft_putchar(char c);
void				parse_file(char *filename);
char				*dynamic(char **array, int width, int lines);
char				get_empty(char *str);
char				get_obstacle(char *str);
char				get_replacement(char *str);
int					**matrix_converter(int **array, int width, int lines);
void				ft_putnbr(int nb);
void				free_memory(int **array, int lines, int fd);
void				form_output(char *filename, int *x, int width, int lines);
int					set_read_pointer(char *filename);
char				*read_line(int fd, unsigned int width);
int					check_file(char *filename);
int					check_folder(char *filename, int b);
#endif
