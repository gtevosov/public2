/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vpupkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 15:34:20 by vpupkin           #+#    #+#             */
/*   Updated: 2017/10/30 16:24:10 by vpupkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_atoi(char *str)
{
	int	i;
	int n;
	int numb;

	i = 0;
	n = 0;
	numb = 0;
	while ((str[i] == '\t') || (str[i] == '\v') || (str[i] == '\n') ||
				(str[i] == '\f') || (str[i] == '\r') || (str[i] == ' '))
		i++;
	if (str[i] == '-')
		n = 1;
	if (str[i] == '-' || str[i] == '+')
		i++;
	while ((str[i] >= '0') && (str[i] <= '9'))
	{
		numb = 10 * numb + (str[i] - '0');
		i++;
	}
	if (n == 1)
		return (-numb);
	return (numb);
}
