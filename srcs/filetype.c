/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filetype.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vpupkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 23:26:00 by vpupkin           #+#    #+#             */
/*   Updated: 2017/11/15 23:31:51 by vpupkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char		get_empty(char *str)
{
	char	c;

	c = str[0];
	return (c);
}

char		get_obstacle(char *str)
{
	char	c;

	c = str[1];
	return (c);
}

char		get_replacement(char *str)
{
	char	c;

	c = str[2];
	return (c);
}
