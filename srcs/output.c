/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   output.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vpupkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/15 18:51:43 by vpupkin           #+#    #+#             */
/*   Updated: 2017/11/15 23:42:40 by vpupkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_header.h"

void		catloop(void)
{
	char	c;

	while (read(0, &c, 1))
		write(1, &c, 1);
}

char		*read_out_line(int fd, char *buf, unsigned int width)
{
	if (read(fd, buf, (width + 1)) > 0)
		return (buf);
	else
	{
		ft_putstr("Error reading lines!");
		exit(-1);
	}
}

void		form_output(char *filename, int *x, int width, int lines)
{
	int		i;
	int		j;
	int		fd;
	char	rp;
	char	*array;

	rp = get_replacement(get_params(filename, 2));
	fd = set_read_pointer(filename);
	array = (char *)malloc(sizeof(char*) * (width + 1));
	i = 0;
	while (i < lines)
	{
		array = read_out_line(fd, array, width);
		j = 0;
		while (j < width)
		{
			if ((i >= x[2] && i < x[2] + x[0])
					&& (j >= x[1] && j < x[1] + x[0]))
				array[j] = rp;
			j++;
		}
		ft_putstr(array);
		i++;
	}
}
