/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_params.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vpupkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/15 14:11:43 by vpupkin           #+#    #+#             */
/*   Updated: 2017/11/15 23:28:12 by vpupkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_header.h"

void			free_memory(int **array, int lines, int fd)
{
	int		i;

	i = 0;
	while (i < lines)
	{
		free(array[i]);
		i++;
	}
	if (close(fd) == -1)
		ft_putstr("close() error");
}

int				check_folder(char *filename, int b)
{
	if (read(b, 0, 0) < 0)
	{
		ft_putstr("bsq: ");
		ft_putstr(filename);
		ft_putstr(": Is a directory\n");
		return (-1);
	}
	else
		return (0);
}

void			print_nofile(char *filename)
{
	ft_putstr("bsq: ");
	ft_putstr(filename);
	ft_putstr(": No such file or directory\n");
}

int				check_file(char *filename)
{
	int		fd;
	int		red;
	char	buf[1];

	red = 0;
	fd = open(filename, O_RDONLY);
	red = read(fd, buf, 1);
	if (fd == -1)
	{
		ft_putstr("bsq: ");
		ft_putstr(filename);
		ft_putstr(": No access to file\n");
		return (-1);
	}
	if (check_folder(filename, fd) < 0)
		return (-1);
	if (close(fd) == -1)
	{
		ft_putstr("close() error");
		return (-1);
	}
	return (1);
}
