/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dynamic.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vpupkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 11:56:43 by vpupkin           #+#    #+#             */
/*   Updated: 2017/11/15 23:31:22 by vpupkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_header.h"

char					*read_line(int fd, unsigned int width)
{
	char				*buf;

	buf = malloc(sizeof(char) * (width + 1));
	if (read(fd, buf, (width + 1)) > 0)
	{
		return (buf);
	}
	else
	{
		ft_putstr("Error, nothing to print");
		exit(-1);
	}
}

int						*convert(char *str, t_parse_settings data)
{
	unsigned int		i;
	int					*line;

	line = malloc(sizeof(int) * (data.width + 1));
	i = 0;
	while (i < data.width)
	{
		if (str[i] == data.em)
			line[i] = 1;
		else if (str[i] == data.ob)
			line[i] = 0;
		else
		{
			ft_putstr("File validation failed!\n");
			exit(-1);
		}
		i++;
	}
	return (line);
}

int						count_pos_value(int **array, int i, int j)
{
	int		a;
	int		b;
	int		c;
	int		res;

	if (array[i][j] == 0)
		return (0);
	else if (i == 0)
		return (array[i][j]);
	else if (j == 0)
		return (array[i][j]);
	a = array[i][j - 1];
	b = array[i - 1][j - 1];
	c = array[i - 1][j];
	res = min_var(a, b, c) + array[i][j];
	return (res);
}

t_parse_settings		get_file_description(char *filename)
{
	t_parse_settings	data;

	data.lines = ft_atoi(get_params(filename, 1));
	data.width = get_width(filename);
	data.em = get_empty(get_params(filename, 2));
	data.ob = get_obstacle(get_params(filename, 2));
	data.rp = get_replacement(get_params(filename, 2));
	data.fd = set_read_pointer(filename);
	return (data);
}

void					parse_file(char *filename)
{
	t_parse_settings	data;
	int					*x;
	int					**array;
	char				*row;
	int					i;

	i = 0;
	if (check_file(filename) == 1)
	{
		data = get_file_description(filename);
		array = create_empty_array(data.width, data.lines);
		row = malloc(sizeof(char) * (data.width + 1));
		while (i < data.lines)
		{
			row = read_line(data.fd, data.width);
			array[i] = convert(row, data);
			i++;
		}
		array = matrix_converter(array, data.width, data.lines);
		i = 0;
		x = find_bsq(array, data.width, data.lines);
		free_memory(array, data.lines, data.fd);
		form_output(filename, x, data.width, data.lines);
	}
}
